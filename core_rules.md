---
title: Core Rules
---

::: chapter :::
## The Fundamentals

:::: block ::::
### Introduction

::::: icon ::::
![](icons/emojione-monotone--teacup-without-handle.svg)
::::

Hello, and welcome to the Uune Roleplaying Game! Uune is a tabletop meta-RPG designed to work with a variety of styles and genres. It is a game where you work as a team to tell a story, overcoming challenges by pooling your resources and rolling dice.

In the sections that follow, I will do my best to explain the fundamental rules and mechanics of the game, and provide guidance whenever necessary to clarify how they are intended to be used.

::::: sub-block ::::
#### Context Clues
In this guide, special contextual hints are given via the use of _italics_ and **bold** styling. Keywords which are intended to be found when skimming for specific rules are boldened, while keywords referencing mechanics that are defined in future sections are italicized.
:::::

::::

:::: block ::::
### What You'll Need

::::: icon :::::
![](icons/emojione-monotone--gear.svg)
:::::

To get started, make sure you have the following items:

* A standard set of six sided dice (5 dice is most common, but more or less will work)
* A coin (optional)
* A notebook or character sheet (available [here](https://uune.org/downloads.html) or provided by your GM)
* At least 1 other person, preferably a few more

::::

:::: block ::::
### The Game Master

::::: icon :::::
![](icons/emojione-monotone--eye-in-speech-bubble.svg)
:::::

The Game Master (or GM) is the player who runs the game. They guide the story and act as the eyes and ears of the _player characters_. The GM also acts as all of the non-player characters (NPCs) in the story, such as shopkeepers, guards, soldiers, kings, etc.

::::

:::: block ::::
### The Setting

::::: icon :::::
![](icons/emojione-monotone--books.svg)
:::::

Uune is a meta-RPG, meaning that it's designed to work with a variety of game styles and genres. Your GM will have a setting picked out, or a custom one made for your table. Some example settings are available [here](https://uune.org/downloads.html)

::::
:::

::: chapter :::
## Characters

:::: block ::::
### Player characters

::::: icon :::::
![](icons/emojione-monotone--memo.svg)
:::::

Everyone other than the GM plays as their own, singular character, known as a player character (or PC).

To build a character, follow the guide below:

1. Brainstorm an idea
2. Get yourself a notepad or a pre-made character sheet
3. Write a name and backstory
4. Choose two Bonds that pertain to your backstory

::::

:::: block ::::
### Levels and Experience

::::: icon :::::
![](icons/emojione-monotone--1st-place-medal.svg)
:::::

Levels represent your character's overall experience, they're an easy way of keeping track of how many _features_ (mainly abilities) they're supposed to have. You _level up_ whenever you gain enough total experience (XP) to reach the next level, and you gain XP by interacting with certain game mechanics. See the following list for level XP:

```{.level-scale start-xp="25" max-level="10"}
```

When you **level up**, you may add 2 _features_ to your character, chosen from those available. By default, Uune gives you 3 feature types: _skill levels_, _boons_, and _banes_. Your setting may add additional options that you can take from as well.

Levels start at 0 (where you have no features added to your character), and usually go up to 10, but can go beyond if your GM wishes. Your GM may want to start you off with a level or two in order to help flesh out your character, but there is no requirement to do so.

::::

:::: block ::::
### Bonds and Story Arcs

::::: icon :::::
![](icons/emojione-monotone--footprints.svg)
:::::

**Bonds** are things which are particularly important to your character's story. Your setting will have specific types of bonds which you can take and adapt to your character's needs. You start with 2 bonds when you first build your character, regardless of level

**Story arcs** are larger sections of plot taking place in the game. They can relate to something your character is doing, or your party as a whole. For example, an arc might pertain to your party solving a mystery, and would be concluded when the mystery itself is solved.

At the end of each arc, you may reflect on the changes your character has had and choose a new bond to take.

::::: sub-block :::::
#### Achievements
Each bond also comes with achievements you can complete by bringing your bond into the story. At the end of each arc, you gain XP from each bond based on the number of achievements you've completed: 5 for one, 10 for two, 20 for three. You then reset your completed achievements for the next arc of the story.
:::::

::::

:::: block ::::
### Features

::::: icon :::::
![](icons/emojione-monotone--hammer-and-wrench.svg)
:::::

Features are the nuts and bolts of character creation and progression. They allow you to add on new abilities, or to strengthen existing ones as your character grows and learns throughout their travels.

::::: sub-block :::::
#### Skills
Skills allow you to boost your rolls for things that you have knowledge around. Whenever you use a _resource,_ you may choose a skill that is related to that resource. You may then add a bonus to said roll equal to the levels you've put into that skill.

Each skill is comprised of one or two words that give a gist of what it is for, and the number of levels you've put into it. You can have whatever skills you can think of, so long as your GM approves it beforehand.

Many settings will have some example skills to get you started, as well as guidelines for what sort of skills would make sense for the world that the game takes place in.
:::::

::::: sub-block :::::
#### Boons
Boons represent specific bits of experience, status, or ability for your character. Each one gives you an extra ability you can use to help you along your way.

Each setting will have it's own set of boons for you to take whenever you level lp.
:::::

::::: sub-block :::::
#### Banes
Banes represent things about your character that are a detriment to them, such as vices, curses, disabilities, etc. When you take a bane, you will gain a detrimental mechanic that kicks in under certain circumstances. Whenever you are affected by this mechanic, you gain _karma_.

Each setting will have it's own set of banes from which you can take.
:::::

::::

:::: block ::::
### Items

::::: icon :::::
![](icons/emojione-monotone--briefcase.svg)
:::::

Your character has 5 **inventory** slots that they can store items in, representing the number of items they can carry, pocket, etc.

::::: sub-block :::::
#### Item Properties
Certain items will have special properties that change how they can be used. These act as quick notes for item specific mechanics. Some base properties are as follows:

* **High Quality:** You gain _advantage_ on _resource_ rolls with this item.
* **Low Quality:** You gain _disadvantage_ on resource rolls with this item.
* **Broken:** Every resource roll with this item has a -3 modifier.
* **Hodge Podge:** This item has a limited number of uses. Roll a _risk_ for every use, giving it the broken property on a complication.
* **Ammunition:** This item uses ammo, which is spent when rolling for a resource with it. roll for the ammo used, not the item itself when using it to fire ammunition.
* **Container:** This item holds a specified number of other items, such as a backpack or box.
* **Consumable:** This item's main usage consumed it in some way, using it up.
:::::

::::

:::

::: chapter :::
## Rolling The Dice

:::: block ::::
### Challenges

::::: icon :::::
![](icons/emojione-monotone--mountain.svg)
:::::

There will be times when your character will have obstacles in their path, whether physical or metaphorical. We call those challenges. Challenges are specific to a single character, though there are ways that other characters can get involved that we'll get into later.

Every challenge has a **difficulty level** which is a number representing the amount of effort required to complete the challenge. For example, a difficulty of zero means that no roll is needed, while a difficulty of 30 means that your task is extremely difficult.

Your **dice pool** is the total amount from all the dice you've rolled towards beating your challenge. You add to your dice pool by making use of resources, which will be explained in their own section. You'll need to get your dice pool to a number equal to or greater than your challenge's difficulty level in order to complete your challenge.

::::: tip :::::
#### GM's Tip!

You can use the the list below to help narrow in on an appropriate difficulty level.

| Difficulty | Level |
|:-----------|------:|
| Effortless |     0 |
| Easy       |     5 |
| Medium     |    10 |
| Difficult  |    15 |
| Hard       |    20 |
| Merciless  |    25 |
| Impossible |    30 |

Having too many rolls can bog the game down, but having too few leaves players wondering what their abilities are good for. To help determine what necessitates a roll, ask yourself the following questions and increase the difficulty of the challenge in proportion to how many obstacles and intricacies are present:

* Are they conspicuous?
* Do they need to move a large distance?
* Do they need to be precise?
* Is there an obstacle in their way?
* Is the environment hindering them?
* Do they have a complex goal?

Additionally, you can adjust the difficulty of a challenge if your player changes their goal slightly. For example, your player might decide that in addition to scoping out a spot for them to hide, they also want to plant some tools for them to use later now that they've found somewhere to put them.
:::::

::::

:::: block ::::
### Resources

::::: icon :::::
![](icons/emojione-monotone--light-bulb.svg)
:::::

Resources are dice which represent your ability to use things at your disposal. You use resources to help you complete a challenge, and using a resource allows you to roll a six-sided die and add it to your current dice pool towards said challenge.

Every time you roll a resource, you gain 1 point of experience.

There are 5 main types of resources, as outlined below. Features might also grant you additional ways to gain or use resources.

::::: sub-block :::::
#### Stamina
The most basic of resources are those inherent to you, your mental and physical fortitude. Your character starts with 5 points of stamina. They may expend a point of stamina to use it as a resource. You regain 1 stamina for every hour that your character sleeps, up to 5.
:::::

::::: sub-block :::::
#### Karma
Another type of resource that you can have is karma. You can only ever have 1 karma at any given time, and you can gain karma from banes, risk, or other mechanics.

You may spend your karma at any time to give yourself a resource for free. Gaining a resource in this way allows you to very slightly alter your circumstances in your favor in order to give you a one-time resource.
:::::

::::: sub-block :::::
#### Stuff You Have
Next up are the things you have on you. That can be things in your inventory, or things in your environment that you readily have access to. If something is a little out of reach or stowed away in a bag, you'll need to roll the resource with _disadvantage_.
:::::

::::: sub-block :::::
#### Stuff You Know
Pieces of information or past experience can also count as resources for you. You might know about about the guard rotation for a heist, or have overheard the duke planning to start a war, or have been trained in bomb diffusal, etc, etc, etc. If it's been established and your GM approves, you can use these sorts of know-how to help you.
:::::

::::: sub-block :::::
#### Stuff You Can Make Up
Finally, you can always try to just make it up as your go along! A little creative thinking and ingenuity can fill a lot of gaps where you might not otherwise be able to progress easily.

* Don't have armor? That big rock might be able to give you cover.
* Not supposed to be somewhere? Lie about it and say that you're doing a job.
* Need something that probably exists nearby? Make a _luck check_ for it!

Your GM will need to be convinced first that it'd make sense, second that it's doable (luck checks help a lot here), and finally they might impose a _risk roll_ if it's iffy.
:::::

::::

:::: block ::::
### Resource Modifiers

::::: icon :::::
![](icons/emojione-monotone--balance-scale.svg)
:::::

At times your GM may apply one of the following modifier mechanics to a resource roll, depending on the nature of the resource and how it's being used.

::::: sub-block :::::
#### Advantage and Disadvantage
Resources aren't always easy to use, nor are they all equally helpful. Sometimes, your GM will impose advantage or disadvantage on the roll.

**Advantage** means that you will need to roll twice, and take the higher roll.

**Disadvantage** means that you will need to roll twice, but take the higher roll.

Having both advantage and disadvantage on a roll means that they cancel out, and the roll is normal.
:::::

::::: sub-block :::::
#### Time
Sometimes, rolling for a resource once just isn't enough. If it makes sense, your GM can grant advantage on it if your character takes double the time it normally would to use it
:::::

::::: sub-block :::::
#### Risk
When the actions you take can lead to unintended side effects, your GM may ask you to roll for risk. Often, this is due to using a resource that you \"made up\" (such as a lie), but can also be a consequence for using a tool in the wrong way or other particular circumstance (such as using a walkie-talkie while trying to be quiet).

Roll your risk die, and add a relevant skill bonus to your roll.

* If you roll a 4 or below, a complication arises that the GM chooses based on the circumstance. This complication shouldn't halt your ability to finish your challenge completely, but might make things more difficult.
* If you roll a 5 or above: no immediate consequence is had, and you gain karma if you don't already have it.
:::::

::::

:::: block ::::
### Quality

::::: icon :::::
![](icons/emojione-monotone--direct-hit.svg)
:::::

Another facet of challenges is how well they are completed. There are 3 levels of Quality for every challenge, based on how large your Dice Pool is relative to the difficulty you needed to beat:

|                                                       |                     |
|:------------------------------------------------------|:--------------------|
| Dice Pool is equal to or slightly past the difficulty | Adequate Success    |
| Dice Pool is 2 times the difficulty                   | Flawless Success    |
| Dice Pool is 3 times the difficulty                   | Fantastical Success |

In any level of success, your character completes what they set off to do. However, in the case of a flawless or fantastical success, your GM may give an additional perk or bonus depending on the challenge. For example, if your character is crafting a weapon and they get a flawless success, your GM might give it the high quality property to reflect the craftsmanship put into it.
::::

:::: block ::::
### Luck

::::: icon :::::
![](icons/emojione-monotone--star.svg)
:::::

**Luck checks** are used for whenever something needs to be answered purely by chance. For example, your GM might call for a luck check to see if something exists in the environment around you that they hadn't explicitly planned for. As another example, they might also use it to see if your character remembered to do something in retrospect that seems plausible but isn't a given.

When your GM asks you to make a luck check, flip a coin. On heads, the situation is in your favor. On tails, it is not in your favor. If you don't have a coin handy, you can instead roll a d6, treating a 4 or higher as a success.

::::

:::

::: chapter :::
## Characters in Peril

:::: block ::::
### Attacking and Defending

::::: icon :::::
![](icons/emojione-monotone--shield.svg)
:::::

1. The attacker will declare their intention to harm the defender to the GM.
2. If the defender is aware of the attack, they may roll resources to defend. This will set the difficulty level of the attacker's challenge.
3. The attacker then has a challenge to complete the attack:
    * For an adequate success, it counts as a light attack and the defender gain a level of the _strained condition_.
    * For a flawless success, it counts as a heavy attack and the defender gains a level of the _gashed condition_ instead.
    * For a fantastical success, it counts as a deadly attack and the defender gains the _helpless condition_. This doesn't necessarily mean that the defender dies (though the attacker may chose to have this be the case), rather it means that they are effectively out of the fight.

::::

:::: block ::::
### Crunch Time

::::: icon :::::
![](icons/emojione-monotone--pistol.svg)
:::::

At times, your GM will need to slow the game down to allow everyone to get in on the action. We call that crunch time.

Starting off, crunch time is always initiated with the actions of a character. Then, the other characters involved can each take their **turn**, completing 1 challenge on their turn. Once everyone has taken their turn, a new **round** begins and everyone can take take their turns again.

If a character wishes to take another turn within the same round, they may do so, but the difficulty of their challenge will be increased by +5 for each additional turn they take.

::::

:::: block ::::
### Conditions

::::: icon :::::
![](icons/emojione-monotone--broken-heart.svg)
:::::

Conditions are active effects which affect your character. These are given out by your GM as a result of an environmental threat, another character's action, a risk roll, etc. The base conditions are outlined below:

::::: list-table :::::
* - Name
  - Cause
  - Effect
  - Stacking
  - Resolution

* - Strained
  - Sustaining a light injury, overexertion, or other debilitating effect
  - All of your challenges have +1 to their difficulty level
  - Yes
  - You heal 1 level of this for every hour that you rest

* - Gashed
  - Sustaining a significant injury
  - All of your resource rolls are reduced by 1
  - Yes
  - You heal 1 level of this whenever you sleep at least 6 hours

* - Helpless
  - Being incapacitated
  - You cannot use resources
  - No
  - Determined by your GM based on context

* - Held
  - Being tied up, grappled, etc
  - You cannot move
  - No
  - You may attempt to break out with an attack against your holder
:::::

<br>

::::

:::
