function cleanup {
    rm -rf deps/list-table
}

trap cleanup EXIT

function build_markdown {
    pandoc --lua-filter=../deps/list-table/list-table.lua --lua-filter=../assets/filters.lua --template ../assets/printable.tmpl.html -o "$@"
}

rm -rf deps/list-table
git clone https://github.com/pandoc-ext/list-table.git deps/list-table

mkdir -p build/
rm -r build/*

cp assets/rules_printable.css build
cp -r assets/icons build/

cd build

build_markdown core_rules.html ../core_rules.md

cd ..

cp sheets/character_sheet.pdf build/
