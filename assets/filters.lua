function level_xp(block)
    if block.classes[1] == "level-scale" then
        local start_xp = tonumber(block.attributes["start-xp"]) or 0
        local max_level = tonumber(block.attributes["max-level"]) or 0

        local level_list = {}
        local last_xp = start_xp
        local current_xp = start_xp
        local xp_total = start_xp
        local level = 1

        repeat
            table.insert(level_list, tostring(xp_total))
            xp_total = xp_total + last_xp
            last_xp = current_xp
            current_xp = xp_total

            level = level + 1
        until level > max_level

        return pandoc.OrderedList(level_list)
    end
end

return {
    {CodeBlock = level_xp},
}
